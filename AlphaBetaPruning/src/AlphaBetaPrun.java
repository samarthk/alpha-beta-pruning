import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public class AlphaBetaPrun {
	private static final String INPUT_FILENAME = "input.txt";
	private static final String OUTPUT_FILENAME = "output.txt";
	private static final String CAL_FILENAME = "calibration.txt";
	private static final Map<Integer, Character> map;
	static{
		map = new HashMap<Integer, Character>();
		map.put(0, 'A');
		map.put(1, 'B');
		map.put(2, 'C');
		map.put(3, 'D');
		map.put(4, 'E');
		map.put(5, 'F');
		map.put(6, 'G');
		map.put(7, 'H');
		map.put(8, 'I');
		map.put(9, 'J');
		map.put(10, 'K');
		map.put(11, 'L');
		map.put(12, 'M');
		map.put(13, 'N');
		map.put(14, 'O');
		map.put(15, 'P');
		map.put(16, 'Q');
		map.put(17, 'R');
		map.put(18, 'S');
		map.put(19, 'T');
		map.put(20, 'U');
		map.put(21, 'V');
		map.put(22, 'W');
		map.put(23, 'X');
		map.put(24, 'Y');
		map.put(25, 'Z');
		
	}
	
	public static int deep = 0;
	public static void main(String[] args) {
		Game data = readData(INPUT_FILENAME);
		
		char[][] currState = copyArray(data.gameState);
		ScoreNode node = beginGame(currState, data.timeLeft);
		createOutput(node.i, node.j, data.gameState);
	}
	
	private static void createOutput(int i, int j, char[][] gameState){
		finalPlay(i, j, gameState[i][j], gameState);
		applyGravity(gameState);
		String move= String.valueOf(map.get(j)) + String.valueOf(i+1);
		writeFile(gameState, move);
	}
	
	private static void finalPlay(int i, int j, char ch, char[][] gameState){
		if(!isSafe(i, j, gameState.length))
			return;
		if(gameState[i][j] == ch){
			gameState[i][j] = '*';
			finalPlay(i-1, j, ch, gameState);
			finalPlay(i+1, j, ch, gameState);
			finalPlay(i, j-1, ch, gameState);
			finalPlay(i, j+1, ch, gameState);
		}
	}
	
	private static void writeFile(char[][] gameState, String move){
		BufferedWriter bw = null;
		FileWriter fw = null;
		
		try {
			fw = new FileWriter(OUTPUT_FILENAME);
			bw = new BufferedWriter(fw);
			
			bw.write(move);
			if(gameState != null){
				bw.write("\n");
				for(int i = 0; i < gameState.length; i++){
					for(int j = 0; j < gameState.length;j++){
						bw.write(String.valueOf(gameState[i][j]));
					}
					bw.write("\n");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				bw.close();
				fw.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	private static ArrayList<Double> readFromCalibration(){
	File file = new File(CAL_FILENAME);
		Scanner in = null;
		ArrayList<Double> cal = new ArrayList<Double>();
		try {
			in = new Scanner(file);
			while(in.hasNextDouble()){
				cal.add(in.nextDouble());
			}
			return cal;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		finally{
			in.close();
		}
		return null;
	}
	
	private static ScoreNode beginGame(char[][] gameState, double remTime){
		
		Set<ValNode> visitedInMax = new HashSet<ValNode>();
		int alpha = Integer.MIN_VALUE;
		int beta = Integer.MAX_VALUE;
		int x = -1;
		int y = -1;
		ArrayList<ScoreNode> scoreList = new ArrayList<ScoreNode>();
		for(int i = 0; i < gameState.length; i++){
			for(int j = 0; j < gameState.length; j++){
				if (!visitedInMax.contains(new ValNode(i, j, gameState[i][j]))) {
					ScoreNode node = find(gameState, visitedInMax, i, j);
					if (node != null) {
						scoreList.add(node);
					}
				}
			}
		}
		ArrayList<Double> cal = readFromCalibration();
		deep = 3;
		int size = scoreList.size();
		if(size > 441){
			deep = 3;
		}
		else if(size > 121){
			if((cal.get(1))*4 < remTime){
				deep = 4;
			}
		}
		else if(size > 49){
			if((cal.get(2))*4 < remTime){
				deep = 5;
			}
			else{
				deep = 4;
			}
		}
		else if(size > 36){
			if((cal.get(3))*4 < remTime){
				deep = 6;
			}
			else{
				deep = 5;
			}
		}
		else if(size > 16){
			if((cal.get(4))*4 < remTime){
				deep = 7;
			}
			else{
				deep = 6;
			}
		}
		else{
			deep = 8;
		}

		sortDecending(scoreList);
		
		for(ScoreNode node : scoreList){
			char[][] currState = copyArray(gameState);
			play(currState,node.i, node.j);
			int score = 0;
			score += node.score;
			int minVal = minVal(currState, alpha, beta,  1, score);
			
			if (alpha < minVal) {
				x = node.i;
				y = node.j;
				alpha = minVal;
			}
		}
		
		return new ScoreNode(x, y, alpha);
	}
	
	
	private static int maxVal(char[][] gameState, int alpha, int beta, int depth, int score){
		
		if(depth == deep || isGameEnd(gameState))
			return score;
		
		ArrayList<ScoreNode> scoreList = new ArrayList<ScoreNode>();
		Set<ValNode> visitedInMax = new HashSet<ValNode>();
		for(int i = 0; i < gameState.length; i++){
			for(int j = 0; j < gameState.length; j++){
				if (!visitedInMax.contains(new ValNode(i, j, gameState[i][j]))) {
					ScoreNode node = find(gameState, visitedInMax, i, j);
					if (node != null) {
						node.score += score;
						scoreList.add(node);
					}
				}
			}
		}
		
		sortDecending(scoreList);

		for(ScoreNode node : scoreList){
			char[][] currState = copyArray(gameState);
			play(currState, node.i, node.j);
			int minVal = minVal(currState, alpha, beta, depth + 1, node.score);
			alpha = Math.max(minVal, alpha);
			if (alpha >= beta)
				return beta;
		}
		
		
		return alpha;
	}
	
	private static int minVal(char[][] gameState, int alpha, int beta, int depth, int score){
		if(depth == deep || isGameEnd(gameState))
			return score;
		
		Set<ValNode> visitedInMin = new HashSet<ValNode>();
		ArrayList<ScoreNode> scoreList = new ArrayList<ScoreNode>();
		for(int i = 0; i < gameState.length; i++){
			for(int j = 0; j < gameState.length; j++){
				if (!visitedInMin.contains(new ValNode(i, j, gameState[i][j]))) {
					ScoreNode node = find(gameState, visitedInMin, i, j);
					if (node != null) {
						node.score = score - node.score;
						scoreList.add(node);
					}
				}
			}
		}
		
		sortAscending(scoreList);
		
		for(ScoreNode node : scoreList){
			char[][] currState = copyArray(gameState);
			play(currState, node.i, node.j);
			int maxVal = maxVal(currState, alpha, beta, depth + 1, node.score);
			beta = Math.min(maxVal, beta);
			if (beta <= alpha)
				return alpha;
		}
		return beta;
	}
	
	private static void sortAscending(ArrayList<ScoreNode> list){
		list.sort(new Comparator<ScoreNode>() {

			@Override
			public int compare(ScoreNode n1, ScoreNode n2) {
				if(n1.score > n2.score)
					return 1;
				else if(n1.score < n2.score)
					return -1;
				return 0;
			}
		});
	}
	
	private static void sortDecending(ArrayList<ScoreNode> list){
		list.sort(new Comparator<ScoreNode>() {

			@Override
			public int compare(ScoreNode n1, ScoreNode n2) {
				if(n1.score < n2.score)
					return 1;
				else if(n1.score > n2.score)
					return -1;
				return 0;
			}
		});
	}
	
	
	private static boolean isGameEnd(char[][] gameState){
		for(int i = 0; i < gameState.length; i++){
			for(int j = 0; j < gameState.length; j++){
				if(gameState[i][j] != '*')
					return false;
			}
		}
		return true;
	}
	
	private static  char[][] copyArray(char[][] gameState){
		char[][] newArr = new char[gameState.length][gameState.length];
		for(int i = 0; i < newArr.length; i++){
			for(int j = 0; j < newArr.length; j++){
				newArr[i][j] = gameState[i][j];
			}
		}
		
		return newArr;
	}
	
	private static void play(char[][] gameState, int i, int j){
			applyStar(gameState, i, j, gameState[i][j]);
			applyGravity(gameState);
	}
	
	private static ScoreNode find(char[][] gameState, Set<ValNode> visisted, int i, int j){
		if(gameState[i][j] != '*' ){
			int count = countWithoutStar(gameState, i, j, gameState[i][j], visisted);
			ScoreNode node = new ScoreNode();
			node.i = i;
			node.j = j;
			node.score = (count*count);
			return node;
		}
		return null;
	}
	
	private static void applyGravity(char[][] gameState){
		for(int j = 0; j < gameState.length; j++){
			int i = gameState.length - 1;
			int k = gameState.length - 1;
			while(i > -1){
				if(gameState[i][j] == '*'){
					i--;
				}
				else{
					if(i < k){
						char ch = gameState[i][j];
						gameState[i][j] = '*';
						gameState[k][j] = ch;
						i--;
						k--;
					}
					else{
						i--;
						k--;
					}
				}
			}
		}
	}
	
	
	private static int countWithoutStar(char[][] gameState, int i, int j, char ch, Set<ValNode> visisted){
		int count = 0;
		if(!isSafe(i, j, gameState.length))
			return 0;
		
		if(ch == gameState[i][j] && !visisted.contains(new ValNode(i, j, gameState[i][j]))){
			count++;
			ValNode temp = new ValNode();
			temp.i = i;
			temp.j = j;
			temp.val = ch;
			
			visisted.add(temp);
			count += countWithoutStar(gameState, i-1, j, ch, visisted);
			count += countWithoutStar(gameState, i+1, j, ch, visisted);
			count += countWithoutStar(gameState, i, j-1, ch, visisted);
			count += countWithoutStar(gameState, i, j+1, ch, visisted);
		}
		
		return count;
	}
	
	private static void applyStar(char[][] gameState, int i, int j, char ch){
		if(!isSafe(i, j, gameState.length))
			return;
		
		if(ch == gameState[i][j]){
			gameState[i][j] = '*';
			
			applyStar(gameState, i-1, j, ch);
			applyStar(gameState, i+1, j, ch);
			applyStar(gameState, i, j-1, ch);
			applyStar(gameState, i, j+1, ch);
		}
		
	}
	
	private static boolean isSafe(int i, int j, int len){
		if(i >= 0 && i < len && j >= 0 && j < len)
			return true;
		return false;
	}
	
	private static Game readData(String fileName) {
		File file = new File(fileName);
		Game data = new Game();
		Scanner in = null; 
		try {
			in = new Scanner(file);
			if(!in.hasNextInt())
				return null;
			data.size = in.nextInt();
			
			if(!in.hasNextInt())
				return null;
			data.fruitTypes = in.nextInt();
			
			if(!in.hasNextDouble())
				return null;
			data.timeLeft = in.nextDouble();
			
			data.gameState = new char[data.size][data.size];
			for(int i = 0; i < data.size; i++){
				if(!in.hasNext())
					return null;
				String row = in.next();
				for(int j = 0; j < data.size; j++){
					data.gameState[i][j] = row.charAt(j);
				}
			}
			return data;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		finally{
			in.close();
		}
		return null;
	}
}

class Game{
	int size;
	int fruitTypes;
	double timeLeft;
	char[][] gameState;
}

class ScoreNode{
	int i;
	int j;
	int score;
	
	public ScoreNode(){
		
	}
	
	public ScoreNode(int i, int j){
		this.i = i;
		this.j = j;
	}
	
	public ScoreNode(int i, int j, int score){
		this.i = i;
		this.j = j;
		this.score = score;
	}
}

class ValNode{
	int i;
	int j;
	char val;
	
	public ValNode(){
		
	}
	public ValNode(int i, int j, char val){
		this.i = i;
		this.j = j;
		this.val = val;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + i;
		result = prime * result + j;
		result = prime * result + val;
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ValNode other = (ValNode) obj;
		if (i != other.i)
			return false;
		if (j != other.j)
			return false;
		if (val != other.val)
			return false;
		return true;
	}
}